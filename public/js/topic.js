$(document).ready(function(){

  // Sample tab
  var secondTabs = 'Hallo everyone, I am secob tab';
  $('.tabs_tl').find('li').bind('click', function(){
    var that = $(this);
    createLoader(that);
    setTimeout(function() {
      showContent(that, secondTabs);
    }, 1500);
  })

  // Sample pengunaan auto suggestion dan tags
  // Tambahkan class tags_wrap pada wrapper, lalu beri input dan id nya didalam wrapper tsb. Ex: <input id="compare1" />
  var countries = [
    { value: 'Prabowo', data: 'PS' },
    { value: 'Jokowi', data: 'JKW' },
    { value: 'Rocky Gerung', data: 'RG', color: '#269dfa' },
    { value: 'Bang Haji Roma', data: 'RG', color: '#48d2a0' },
    { value: 'Mega Koesni', data: 'MG' }
  ];
  
  $('#compare1').autocomplete({
    lookup: countries,
    onSelect: function (suggestion) {
      addTags(suggestion.value, $(this), suggestion.color);
      $(this).val('').focus();
    }
  })
  .on("keydown", function(e) {
    var code = e.which;
    if ( code == 13 || code == 9 ) {
        e.preventDefault();
        tagsKeyDown(e, $(this))
    } else if (code == 8) {
      removeTagsByBackspace($(this));
    }
  });



  // Sample pengunaan auto suggestion dan tags
  // Tambahkan class tags_wrap pada wrapper, lalu beri input dan id nya didalam wrapper tsb. Ex: <input id="compare1" />
  var countries2 = [
    { value: 'Korupsi E-ktp', data: 'KTP' },
    { value: 'Ramon T Lamas', data: 'RG', color: '#269dfa' },
    { value: 'Bang Haji Roma', data: 'RG', color: '#48d2a0' },
  ];
  
  $('#compare2').autocomplete({
    lookup: countries2,
    onSelect: function (suggestion) {
      addTags(suggestion.value, $(this), suggestion.color);
      $(this).val('').focus();
    }
  })
  .on("keydown", function(e) {
    var code = e.which;
    if ( code == 13 || code == 9 ) {  
      tagsKeyDown(e, $(this))
      e.preventDefault();
    } else if (code == 8) {
      removeTagsByBackspace($(this));
    }
  });

  // Sample remove button
  $('.remove_topic').bind('click', function(){
    $(this).closest('.rounded_choices').remove();
  });

});
