function activeTab(elm) {
  $(elm).closest('.tabs_tl').find('li').removeClass('active_tabs');
  $(elm).addClass('active_tabs');
}

function createLoader(elm) {
  var contentWrapper = $(elm).closest('.tabs_tl').next();
  var loader = $('<div />').addClass('loader');
  var loaderWrapper = $('<div />').addClass('loader_wrapper').append(loader);
  $(contentWrapper).html(loaderWrapper);
}

function showContent(elm, content) {
  var contentWrapper = $(elm).closest('.tabs_tl').next();
  $(contentWrapper).html(content);
  activeTab(elm);
}