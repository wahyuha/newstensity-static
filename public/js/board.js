$( document ).ready(function() {
  $('.collapse').bind("click", function(e) {
    var nextElm = $(this).closest('.board_header').next();
    var btn = $(this);
    if ($(nextElm).is(':hidden')) {
      $(nextElm).slideDown(function(){
        $(btn).css('-webkit-transform','rotate(0deg)'); 
      });
    } else {
      $(nextElm).slideUp(function() {
        $(btn).css('-webkit-transform','rotate(180deg)'); 
      });
    }
  });
  $('.help').bind('click', function(){
    var title = $(this).attr('title');
    var content = $(this).attr('content');
    var elmTitle = $('<div />').addClass('tips_title').html(title);
    var elmContent = $('<div />').addClass('tips_content').html(content);
    var helpElm = $('<div />').addClass('help_tips').append(elmTitle).append(elmContent);
    if (content) {
      $(this).html(helpElm);
      $(window).click(function() {
        $(helpElm).remove();
      });
    }
  });
  $('.help').bind('click', function(e){
    e.stopPropagation();
  });
});