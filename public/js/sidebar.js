$( document ).ready(function() {
  $('.menu_parent').bind("click", function(e) {
    var nextElm = $(this).closest('div').next();
    var menuParent = $(this);
    var upDown = $(menuParent).find('i').last();
    if ($(nextElm).is(':hidden')) {
      $(nextElm).slideDown('fast', function(){
        $(upDown).addClass('icon_up');
        $(upDown).removeClass('icon_down');
      });
    } else {
      $(nextElm).slideUp('fast', function() {
        $(upDown).addClass('icon_down');
        $(upDown).removeClass('icon_up');
      });
    }
  });

  $('.sub_header').bind('click', function(){
    var subList = $(this).closest('.sub_menu').find('ul');
    var upDown = $(this).closest('.sub_menu').find('.icon_dd');
    if ($(subList).is(':hidden')) {
      $(subList).slideDown('fast', function() {
        $(upDown).addClass('icon_up');
        $(upDown).removeClass('icon_down');
      })
    } else {
      $(subList).slideUp('fast', function() {
        $(upDown).addClass('icon_down');
        $(upDown).removeClass('icon_up');
      })
    }
  });

  function clearActiveMenu() {
    $('.menu_list').find('a').removeClass('list_active');
  }

  $('.menu_list').find('a').bind('click', function(){
    const idTarget = $(this).attr('href').split('#')[1]
    $('html, body').animate({
        scrollTop: $('#'+idTarget).offset().top
    }, 1000);
    clearActiveMenu();
    $(this).addClass('list_active');
  });

  clearActiveMenu();
  var hash = window.location.href.split('#')[1];
  if(hash) {
    var ahref = $('.menu_list').find("a[href$="+hash+"]");
    $(ahref).addClass('list_active');
  }
});