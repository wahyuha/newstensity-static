function removeTags(i) {
  $(i).closest('span').remove();
}

function removeTagsByBackspace(inputs) {
  var inputValue = $(inputs).val();
  if (inputValue.length > 0) return;
  var idELm = $(inputs).closest('.tags_wrap').find('span').last();
  removeTags(idELm);
}

function addTags(words, textInput, color) {
  if (words.length === 0) return;
  var i = $('<i/>').bind('click', function(){
    removeTags(this);
  });
  var span = $('<span />').append(words);
  if (color) $(span).css('background-color', color);
  $(span).append(i);
  $(span).insertBefore(textInput);
}

function tagsKeyDown(e, elm) {
  var data = e.target.value;
  addTags(data, $(elm));
  $(elm).val('').focus();
}
